
public class CosmosApp : Gtk.Application {
	construct {
		application_id = "com.cosmos.wallpaper";
	}

	private void run_dbus () {
		Bus.own_name (BusType.SESSION, "com.cosmos.wallpaper",
			BusNameOwnerFlags.NONE,
			on_bus_aquired,
			() => {printerr("[Dbus] (com.cosmos.wallpaper) is running \n");},
			() => printerr("[Dbus] Could not acquire name (com.cosmos.wallpaper)\n"));
	}	

	public static Wallpaper wallpaper;

	public override void activate () {
		// Run the dbus server
		run_dbus ();

		wallpaper = new Wallpaper();
		var win = new Gtk.ApplicationWindow (this) {
			child = wallpaper,
		};

		GtkLayerShell.init_for_window(win);
		GtkLayerShell.set_layer (win, GtkLayerShell.Layer.BACKGROUND);
		// GtkLayerShell.auto_exclusive_zone_enable (win);
		// GtkLayerShell.set_namespace (win, "wallpaper");

		GtkLayerShell.set_margin (win, GtkLayerShell.Edge.BOTTOM, -200);	
		GtkLayerShell.set_margin (win, GtkLayerShell.Edge.LEFT, -200);	
		GtkLayerShell.set_margin (win, GtkLayerShell.Edge.RIGHT, -200);	
		GtkLayerShell.set_margin (win, GtkLayerShell.Edge.TOP, -200);	
		// GtkLayerShell.set_margin (win, GtkLayerShell.Edge.RIGHT, -50);	
		GtkLayerShell.set_anchor (win, GtkLayerShell.Edge.BOTTOM, true);
		GtkLayerShell.set_anchor (win, GtkLayerShell.Edge.TOP, true);

		var monitors = Gdk.Display.get_default ().get_monitors ();
		var rectangle = ((Gdk.Monitor)monitors.get_item (0)).get_geometry ();
		win.set_default_size (rectangle.width, rectangle.height);

		win.present ();
	}
}

int main (string[] args) {
	var app = new CosmosApp ();
	return app.run (args);
}
