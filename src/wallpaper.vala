
public enum WallpaperType {
	NULL = 0,
	PICTURE = 1,
	VIDEO = 2,
	GIF = 3
}

public class Wallpaper : Gtk.Box {
	private Gtk.Picture? picture = null;
	private Settings settings;
	private Gtk.MediaFile? media = null;
	public string path {get; private set;}

	public WallpaperType wallpaper_type {get; private set; default = NULL;}
	public int64 timestamp {get; private set;}

	construct {
		orientation = Gtk.Orientation.VERTICAL;
		spacing = 0;
	}

	/**
	 * Constructor
	 */
	public Wallpaper () {
		try {
			settings = CosmosDesktop.get_config("com.cosmos.wallpaper");
			settings.changed["wallpaper-path"].connect (() => {
				try {
					init_picture (settings.get_string("wallpaper-path"));
				}
				catch (Error e) {
					warning (e.message);
				}
			});
			init_picture (settings.get_string("wallpaper-path"));
		}
		catch (Error e) {
			warning (e.message);
		}
	}

	/**
	 * Get the type of the file
	 *
	 * @param path The path to the file
	 * @return The type of the file (Picture, Video, Gif)
	 */
	private WallpaperType get_type_of_file (string path) throws Error {
		var fs = File.new_for_path(path);
		var content = fs.query_info ("standard::content-type", 0).get_content_type();

		if (content.has_prefix ("image/gif"))
			return WallpaperType.GIF;
		else if (content.has_prefix ("image/webp"))
			return WallpaperType.NULL; // WebP is not supported (Gtk4 don't support it)
		else if (content.has_prefix ("image/"))
			return WallpaperType.PICTURE;
		else if (content.has_prefix ("video/"))
			return WallpaperType.VIDEO;
		return WallpaperType.NULL;
	}


	/**
	 * Initialize the wallpaper
	 *
	 * @param wallpaper_path The path to the wallpaper
	 * @return True if the wallpaper was successfully initialized
	 */
	private void init_picture (string wallpaper_path) throws Error {
		// Remove the previous picture if it exists
		if (picture != null) {
			remove (picture);
			picture = null;
			media = null;
		}

		// get the type of the file (Video, Picture, Gif)
		var type_of_file = get_type_of_file (wallpaper_path);

		if (type_of_file != NULL && FileUtils.test (wallpaper_path, FileTest.EXISTS)) {

			this.wallpaper_type = type_of_file;

			switch (type_of_file) {

				case VIDEO:
					media = Gtk.MediaFile.for_filename (wallpaper_path);
					this.bind_property ("timestamp", media, "timestamp");
					media.set_playing (true);
					media.set_loop (true);
					media.set_muted (true);
					picture = new Gtk.Picture.for_paintable(media);
					break;

				case GIF:
					var gif = new CosmosDesktop.Gif (wallpaper_path);
					picture = new Gtk.Picture.for_paintable(gif);
					break;

				case PICTURE:
					picture = new Gtk.Picture.for_filename (wallpaper_path);
					break;

				default:
					break;
			}
			path = wallpaper_path;
		}

		/* Use default system choice if picture is not loaded */
		if (picture == null) {
			if (FileUtils.test ("/etc/gde/config/wallpaper", FileTest.EXISTS)) {
				picture = new Gtk.Picture.for_filename (Config.APPDIR + "/config/wallpaper");
				path = Config.APPDIR + "/config/wallpaper";
				wallpaper_type = WallpaperType.PICTURE;
			}
			else {
				wallpaper_type = WallpaperType.NULL;
				throw new CosmosDesktop.CosmosError.NOT_FOUND ("Wallpaper not found");
			}
		}

		picture.hexpand = true;
		picture.vexpand = true;

		picture.content_fit = Gtk.ContentFit.COVER;

		base.append (picture);
		service.wallpaper_changed(type_of_file, path);
	}
}

