[DBus(name = "com.cosmos.wallpaper")]
public class Service : Object {

	public signal void wallpaper_changed (WallpaperType type, string path);

	public WallpaperType get_wallpaper_type() throws Error {
		return CosmosApp.wallpaper.wallpaper_type;
	}

	public int64 get_timestamp () throws Error {
		return CosmosApp.wallpaper.timestamp;
	}

	public string get_path () throws Error {
		return CosmosApp.wallpaper.path;
	}

}


public Service service;

public void on_bus_aquired (DBusConnection conn) {
    try {
        service = new Service();
        conn.register_object ("/com/cosmos/wallpaper", service);
    } catch (IOError e) {
        stderr.printf ("Could not register service: %s\n", e.message);
    }
}

